package com.sms.rubenzamorano.gatewayiot.Retrofit.Events;


import com.sms.rubenzamorano.gatewayiot.Retrofit.GateWaySMS_Callback;
import com.sms.rubenzamorano.gatewayiot.Retrofit.Response.ResponseSMS;

/**
 * Created by ruben on 12/02/17.
 */

public class GateWayEvent {

    private ResponseSMS serverResponse;

    public GateWayEvent(ResponseSMS serverResponse, final GateWaySMS_Callback callback) {
        this.serverResponse = serverResponse;
        callback.onSuccess(serverResponse);
    }

    public ResponseSMS getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(ResponseSMS serverResponse) {
        this.serverResponse = serverResponse;
    }


}

package com.sms.rubenzamorano.gatewayiot.Retrofit;


import com.sms.rubenzamorano.gatewayiot.Retrofit.Events.ErrorEvent;
import com.sms.rubenzamorano.gatewayiot.Retrofit.Events.GateWayEvent;
import com.sms.rubenzamorano.gatewayiot.Retrofit.Response.ResponseSMS;
import com.squareup.otto.Produce;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dori on 12/28/2016.
 */

public class Communicator {
    private static final String TAG = "CommunicatorRestaurante";
    //rivate static final String SERVER_URL = "http://50.23.85.50:12";
    private static final String SERVER_URL = "http://tkcore.ddns.net:91";

    //final Gson gson = new Gson();


    public void InsertSMS(String cNumero, String cMensaje, final GateWaySMS_Callback callback) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();

        Interface service = retrofit.create(Interface.class);

        Call<ResponseSMS> call = service.post(cNumero,cMensaje);

        call.enqueue(new Callback<ResponseSMS>() {
            @Override
            public void onResponse(Call<ResponseSMS> call, Response<ResponseSMS> response) {
                // response.isSuccessful() is true if the response code is 2xx
                BusProvider.getInstance().post(new GateWayEvent(response.body(), callback));
            }

            @Override
            public void onFailure(Call<ResponseSMS> call, Throwable t) {
                // handle execution failures like no internet connectivity
                BusProvider.getInstance().post(new ErrorEvent(-2, t.getMessage()));
            }
        });
    }


    @Produce
    public GateWayEvent produceServerEvent(ResponseSMS serverResponse, GateWaySMS_Callback callback) {
        return new GateWayEvent(serverResponse, callback);
    }

    @Produce
    public ErrorEvent produceErrorEvent(int errorCode, String errorMsg) {
        return new ErrorEvent(errorCode, errorMsg);
    }
}

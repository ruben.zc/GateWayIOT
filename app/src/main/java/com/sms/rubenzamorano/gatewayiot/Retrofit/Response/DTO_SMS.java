package com.sms.rubenzamorano.gatewayiot.Retrofit.Response;

/**
 * Created by rubenzamorano on 15/07/17.
 */

public class DTO_SMS {
    public int nIdMensaje;
    public String cNumero;
    public String cMensaje;

    public DTO_SMS(int nIdMensaje, String cNumero, String cMensaje) {
        this.nIdMensaje = nIdMensaje;
        this.cNumero = cNumero;
        this.cMensaje = cMensaje;
    }

    public int getnIdMensaje() {
        return nIdMensaje;
    }

    public void setnIdMensaje(int nIdMensaje) {
        this.nIdMensaje = nIdMensaje;
    }

    public String getcNumero() {
        return cNumero;
    }

    public void setcNumero(String cNumero) {
        this.cNumero = cNumero;
    }

    public String getcMensaje() {
        return cMensaje;
    }

    public void setcMensaje(String cMensaje) {
        this.cMensaje = cMensaje;
    }
}

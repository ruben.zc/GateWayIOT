package com.sms.rubenzamorano.gatewayiot.Retrofit.Response;

/**
 * Created by rubenzamorano on 15/07/17.
 */

public class ResponseSMS {
    public boolean bError;
    public String cMensaje;
    public String cCodigoError;
    public String cDestinatario;
    public String cSMS;

    public ResponseSMS(boolean bError, String cMensaje, String cCodigoError, String cDestinatario, String cSMS) {
        this.bError = bError;
        this.cMensaje = cMensaje;
        this.cCodigoError = cCodigoError;
        this.cDestinatario = cDestinatario;
        this.cSMS = cSMS;
    }

    public boolean isbError() {
        return bError;
    }

    public void setbError(boolean bError) {
        this.bError = bError;
    }

    public String getcMensaje() {
        return cMensaje;
    }

    public void setcMensaje(String cMensaje) {
        this.cMensaje = cMensaje;
    }

    public String getcCodigoError() {
        return cCodigoError;
    }

    public void setcCodigoError(String cCodigoError) {
        this.cCodigoError = cCodigoError;
    }

    public String getcDestinatario() {
        return cDestinatario;
    }

    public void setcDestinatario(String cDestinatario) {
        this.cDestinatario = cDestinatario;
    }

    public String getcSMS() {
        return cSMS;
    }

    public void setcSMS(String cSMS) {
        this.cSMS = cSMS;
    }
}

package com.sms.rubenzamorano.gatewayiot.Retrofit;


import com.sms.rubenzamorano.gatewayiot.Retrofit.Response.ResponseSMS;

/**
 * Created by ruben on 02/02/17.
 */

public interface GateWaySMS_Callback {

    void onSuccess(ResponseSMS result);
}

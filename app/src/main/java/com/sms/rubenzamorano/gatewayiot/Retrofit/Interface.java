package com.sms.rubenzamorano.gatewayiot.Retrofit;



import com.sms.rubenzamorano.gatewayiot.Retrofit.Response.ResponseSMS;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Dori on 12/28/2016.
 */

public interface Interface {

    @FormUrlEncoded
    @POST("/SMS.asmx/InsertSMS")
    Call<ResponseSMS> post(
            @Field("cNumero") String cNumero,
            @Field("cMensaje") String cMensaje
    );

    @GET("/sp/index.php")
    Call<ResponseSMS> get(
            @Query("method") String method,
            @Query("username") String username,
            @Query("password") String password
    );

}
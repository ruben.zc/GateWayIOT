package com.sms.rubenzamorano.gatewayiot.GateWay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.sms.rubenzamorano.gatewayiot.Retrofit.Communicator;
import com.sms.rubenzamorano.gatewayiot.Retrofit.GateWaySMS_Callback;
import com.sms.rubenzamorano.gatewayiot.Retrofit.Response.ResponseSMS;


public class IncomingSms extends BroadcastReceiver implements GateWaySMS_Callback {
	
	// Get the object of SmsManager
	final SmsManager sms = SmsManager.getDefault();
	final Communicator communicator = new Communicator();
	public Context mContex;

	
	public void onReceive(Context context, Intent intent) {

		if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
			Intent activityIntent = new Intent(context, MainActivity.class);
			activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(activityIntent);
		}
//        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
//            Intent serviceIntent = new Intent(context, BroadcastNewSms.class);
//            context.startService(serviceIntent);
//        }

		// Retrieves a map of extended data from the intent.
		final Bundle bundle = intent.getExtras();
		mContex = context;



		try {
			
			if (bundle != null) {
				
				final Object[] pdusObj = (Object[]) bundle.get("pdus");
				
				for (int i = 0; i < pdusObj.length; i++) {
					
					SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
					String phoneNumber = currentMessage.getDisplayOriginatingAddress();
					
			        String senderNum = phoneNumber;
			        String message = currentMessage.getDisplayMessageBody();

			        Log.i("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);
			        


					communicator.InsertSMS(senderNum,message, this);

					//Log.d("","senderNum: "+ senderNum + ", message: " + message);
					
				} // end for loop
              } // bundle is null

		} catch (Exception e) {
			Log.e("SmsReceiver", "Exception smsReceiver" +e);
			
		}
	}


	@Override
	public void onSuccess(ResponseSMS result) {
//		int duration = Toast.LENGTH_LONG;
//		//Toast toast = Toast.makeText(mContex, "senderNum: "+ senderNum + ", message: " + message, duration);
//		Toast toast = Toast.makeText(mContex, result.getcMensaje(), duration);
//		toast.show();
		Log.i("ResponseSMS", result.getcCodigoError());

		if(!result.isbError())
		{
			try {
				SmsManager smsManager = SmsManager.getDefault();
				smsManager.sendTextMessage(result.getcDestinatario(), null, result.getcSMS(), null, null);
//				Toast.makeText(getApplicationContext(), "Message Sent",
//						Toast.LENGTH_LONG).show();
				Log.d("SMS","Enviado");
			} catch (Exception ex) {
//				Toast.makeText(getApplicationContext(),ex.getMessage().toString(),
//						Toast.LENGTH_LONG).show();
				Log.d("SMS","No Enviado");
				ex.printStackTrace();
			}

		}


	}
}